import sys
import PyQt5.QtWidgets as qt
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvas


class mywindow(qt.QMainWindow):
    def __init__(self):
        # Set up the window GUI using the import Ui_MainWindow
        super(mywindow, self).__init__()
        main_layout = qt.QHBoxLayout()
        self.setWindowTitle("Template")

        # Add Buttons
        button_layout = qt.QVBoxLayout()  # make a layout for buttons (vertical table)
        self.clear = qt.QPushButton("Clear")  # make a clear button
        # Now the buttons have been made, add them to the button layout
        button_layout.addWidget(self.clear)
        main_layout.addLayout(button_layout)  # add the button layout to the main layout

        # Make required parts for matplotlib: figure, canvas and axes
        self.fig = Figure()
        self.canvas = FigureCanvas(self.fig)
        self.ax = self.fig.subplots(1, 1)

        # Add matplotlib elements to the layout
        main_layout.addWidget(self.canvas)

        # Plot some results onto the matplotlib axis and draw them
        self.ax.plot([1, 2, 3], '-o')
        self.canvas.draw()


        # Add a clicked event to the clear button, the method 
        # self.clear_clicked (defined later) will be run when the clear
        # button is clicked
        self.clear.clicked.connect(self.clear_clicked)

        # Make the main layout show up in the window
        self.setCentralWidget(qt.QWidget(self))
        self.centralWidget().setLayout(main_layout)


    def clear_clicked(self):
        '''
        Clear the axis and plot new data
        '''
        self.ax.clear()  # clear all data on the axis
        self.ax.plot([2, 1, 7], 'C1-s')  # add some new points
        self.canvas.draw()  # draw the canvas


if __name__ == '__main__':
    # This section should almost always be the same, it just makes a
    # mywindow object and runs it
    app = qt.QApplication(sys.argv)
    window = mywindow()
    window.show()
    app.exec()