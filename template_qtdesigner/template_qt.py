import sys
import PyQt5.QtWidgets as qt
from main_window import Ui_MainWindow  # This is the python file output by pyuic5 from qtdesigner
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvas


class mywindow(qt.QMainWindow):
    def __init__(self):
        # Set up the window GUI using the import Ui_MainWindow
        super(mywindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        # Make required parts for matplotlib: figure, canvas and axes
        self.fig = Figure()
        self.canvas = FigureCanvas(self.fig)
        self.ax = self.fig.subplots(1,1)
        
        # Add matplotlib elements to the layout
        self.ui.horizontalLayout.addWidget(self.canvas,-1)
        
        # Plot some results onto the matplotlib axis and draw them
        self.ax.plot([1,2,3], '-o')
        self.canvas.draw()
        
        # Add a clicked event to the clear button, the method 
        # self.clear_clicked (defined later) will be run when the clear
        # button is clicked
        self.ui.clear.clicked.connect(self.clear_clicked)


    def clear_clicked(self):
        '''
        Clear the axis and plot new data
        '''
        self.ax.clear()  # clear all data on the axis
        self.ax.plot([2,1,7], 'C1-s')  # add some new points
        self.canvas.draw()  # draw the canvas


if __name__ == '__main__':
    # This section should almost always be the same, it just makes a
    # mywindow object and runs it
    app = qt.QApplication(sys.argv)
    window = mywindow()
    window.show()
    app.exec()