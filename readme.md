## Python GUI Demo with PyQT5 and Templates

The folder layout of this repository is:
- **presentation/** - a presentation about how to make GUI's using
PyQT5 and QTDesigner, and the files for the presented demonstration. The
presentation was made using Latex and the source files are included.
- **template_qtdesigner/** - a template for building GUIs with PyQT5 and
QTDesigner.
- **template_code/** - a template for building GUIs with PyQT5 using only python
code.